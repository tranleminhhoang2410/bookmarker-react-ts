import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { ROUTES } from './constants';
import MainLayout from './layouts/MainLayout';

const PlaceholderPage = ({ pageName = 'Placeholder' }: { pageName?: string }) => <h1>This is the {pageName} Page</h1>;

const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route element={<MainLayout />}>
          <Route path={ROUTES.HOME} element={<PlaceholderPage pageName="Home" />} />
          <Route path={ROUTES.BOOK_DETAILS} element={<PlaceholderPage pageName="Book details" />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
};

export default App;
