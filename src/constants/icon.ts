import { CreateIcon, DeleteIcon } from '@/components/Icon';

export const ICON_MAPPING = {
  CREATE_ICON: CreateIcon,
  DELETE_ICON: DeleteIcon,
};
