export interface Icon extends React.SVGAttributes<HTMLOrSVGElement> {
  isActive?: boolean;
  size?: number;
  color?: string;
}
