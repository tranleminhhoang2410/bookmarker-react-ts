// External Libraries
import { Outlet } from 'react-router-dom';

// Components
import Header from '@/components/Header';

const MainLayout = () => {
  return (
    <>
      <Header />
      <main className="container m-auto py-8">
        <Outlet />
      </main>
    </>
  );
};

export default MainLayout;
