import { Meta, StoryObj } from '@storybook/react';

// Components
import Typography from '.';

const meta: Meta<typeof Typography> = {
  title: 'Components/Typography',
  component: Typography,
  argTypes: {
    variant: {
      control: 'radio',
      options: ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'paragraph'],
    },
    size: {
      control: 'radio',
      options: ['lg', 'md', 'sm', 'xs'],
    },
  },
  tags: ['autodocs'],
};

export default meta;

type Story = StoryObj<typeof Typography>;

export const Default: Story = {
  args: {
    children: 'This is a text for storybook',
  },
};
