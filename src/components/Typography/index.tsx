import { ReactNode } from 'react';
import { twMerge } from 'tailwind-merge';

interface ITypographyProps {
  variant?: 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6' | 'paragraph' | 'span';
  size?: 'lg' | 'md' | 'sm' | 'xs';
  family?: 'primary' | 'secondary';
  colorScheme?: 'primary' | 'secondary' | 'white';
  className?: string;
  children: ReactNode;
}

const variantClasses = {
  h1: 'font-bold',
  h2: 'font-bold',
  h3: 'font-bold',
  h4: 'font-bold',
  h5: 'font-bold',
  h6: 'font-bold',
  paragraph: '',
  span: '',
};

const sizeClasses = {
  lg: 'text-lg',
  md: 'text-md',
  sm: 'text-sm',
  xs: 'text-xs',
};

const familyClasses = {
  primary: 'font-primary',
  secondary: 'font-secondary',
};

const colorSchemeClasses = {
  primary: 'text-primary',
  secondary: 'text-secondary',
  white: 'text-white',
};

const Typography = ({
  variant = 'paragraph',
  size = 'md',
  family = 'primary',
  colorScheme = 'secondary',
  className = '',
  children,
}: ITypographyProps) => {
  const variantClassName = variantClasses[variant];
  const sizeClassName = size ? sizeClasses[size] : '';
  const familyClassName = familyClasses[family];
  const colorSchemeClassName = colorSchemeClasses[colorScheme];

  const typographyClassName = twMerge(
    variantClassName,
    sizeClassName,
    familyClassName,
    colorSchemeClassName,
    className,
  );

  const Tag = variant === 'paragraph' ? 'p' : variant;

  return <Tag className={typographyClassName}>{children}</Tag>;
};

export default Typography;
