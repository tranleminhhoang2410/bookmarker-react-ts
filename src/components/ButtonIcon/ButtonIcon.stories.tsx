import { Meta, StoryObj } from '@storybook/react';

// Constants
import { ICON_MAPPING } from '@/constants/icon';

// Components
import ButtonIcon from '@/components/ButtonIcon';

// Icons
import { DeleteIcon } from '../Icon';

const meta: Meta<typeof ButtonIcon> = {
  title: 'Components/ButtonIcon',
  component: ButtonIcon,
  argTypes: {
    variant: {
      control: {
        type: 'select',
      },
      options: ['solid', 'outline', 'ghost'],
    },
    size: {
      control: {
        type: 'radio',
      },
      options: ['lg', 'md', 'sm', 'xs'],
    },
    icon: {
      control: {
        type: 'select',
      },
      options: Object.values(ICON_MAPPING),
    },
  },
  tags: ['autodocs'],
};

export default meta;

type Story = StoryObj<typeof ButtonIcon>;

export const Solid: Story = {
  args: {
    variant: 'solid',
    icon: () => <DeleteIcon />,
  },
};

export const Outline: Story = {
  args: {
    variant: 'outline',
    icon: () => <DeleteIcon />,
  },
};

export const Ghost: Story = {
  args: {
    variant: 'ghost',
    icon: () => <DeleteIcon />,
  },
};

export const Disabled: Story = {
  args: {
    variant: 'solid',
    icon: () => <DeleteIcon />,
    disabled: true,
  },
};

export const Loading: Story = {
  args: {
    variant: 'solid',
    icon: () => <DeleteIcon />,
    loading: true,
  },
};
