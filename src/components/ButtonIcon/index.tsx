import Button, { IButtonProps } from '../Button';

interface IButtonIconProps extends Omit<IButtonProps, 'label' | 'leftIcon' | 'rightIcon' | 'children'> {
  icon: (() => React.ReactElement) | null;
}

const ButtonIcon = ({ icon, className, ...otherProps }: IButtonIconProps) => {
  return <Button leftIcon={icon} label="" size="xs" className={className} {...otherProps} />;
};

export default ButtonIcon;
