import { twMerge } from 'tailwind-merge';

// Components
import Spinner from '@/components/Spinner';

export interface IButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  variant?: 'solid' | 'outline' | 'ghost';
  colorScheme?: 'primary' | 'secondary' | 'danger';
  size?: 'lg' | 'md' | 'sm' | 'xs';
  leftIcon?: (() => React.ReactElement) | null;
  rightIcon?: (() => React.ReactElement) | null;
  label?: string;
  onClick?: React.MouseEventHandler<HTMLButtonElement>;
  disabled?: boolean;
  loading?: boolean;
  className?: string;
}

const baseClassName =
  'flex items-center justify-center gap-4 cursor-pointer font-primary font-bold text-sm text-white hover:opacity-60';

const buttonVariantsClassName = {
  solid: (colorScheme: string) => `bg-${colorScheme}`,
  outline: (colorScheme: string) => `bg-transparent border border-${colorScheme} text-${colorScheme}`,
  ghost: (colorScheme: string) => `bg-transparent text-${colorScheme}`,
};

const buttonSizesClassName = {
  lg: 'min-w-48 px-4 py-3 rounded-lg',
  md: 'min-w-40 px-3 py-3 rounded-md',
  sm: 'min-w-32 px-2 py-3 rounded-sm',
  xs: 'min-w-12 px-2 py-3 rounded-xs',
};

const Button = ({
  children,
  variant = 'solid',
  colorScheme = 'primary',
  size = 'md',
  leftIcon: LeftIcon,
  rightIcon: RightIcon,
  label = 'Button',
  disabled = false,
  loading = false,
  className = '',
  ...otherProps
}: IButtonProps) => {
  const variantClassName = buttonVariantsClassName[variant](colorScheme);
  const sizeClassName = buttonSizesClassName[size];
  const gapClassName = !label && 'gap-0';
  const disabledClassName = (disabled || loading) && 'cursor-not-allowed opacity-60';

  const buttonClassName = twMerge(
    baseClassName,
    variantClassName,
    sizeClassName,
    gapClassName,
    disabledClassName,
    className,
  );

  return (
    <button className={buttonClassName} {...otherProps} disabled={disabled || loading}>
      {loading ? (
        <Spinner size="medium" color="light" />
      ) : (
        <>
          {LeftIcon && <LeftIcon />}
          {label && (LeftIcon || RightIcon) ? <span className="text-sm font-primary font-bold">{label}</span> : label}
          {RightIcon && <RightIcon />}
        </>
      )}
    </button>
  );
};

export default Button;
