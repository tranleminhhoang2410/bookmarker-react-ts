import { Meta, StoryObj } from '@storybook/react';

// Constants
import { ICON_MAPPING } from '@/constants/icon';

// Components
import Button from '@/components/Button';

// Icons
import { CreateIcon } from '../Icon';

const meta: Meta<typeof Button> = {
  title: 'Components/Button',
  component: Button,
  argTypes: {
    variant: {
      control: {
        type: 'select',
      },
      options: ['solid', 'outline', 'ghost'],
    },
    size: {
      control: {
        type: 'radio',
      },
      options: ['lg', 'md', 'sm', 'xs'],
    },
    leftIcon: {
      control: {
        type: 'select',
      },
      options: Object.values(ICON_MAPPING),
    },
    rightIcon: {
      control: {
        type: 'select',
      },
      options: Object.values(ICON_MAPPING),
    },
  },
  tags: ['autodocs'],
};

export default meta;

type Story = StoryObj<typeof Button>;

export const Solid: Story = {
  args: {
    variant: 'solid',
    label: 'Button',
  },
};

export const Outline: Story = {
  args: {
    variant: 'outline',
    label: 'Button',
  },
};

export const Ghost: Story = {
  args: {
    variant: 'ghost',
    label: 'Button',
  },
};

export const LeftIcon: Story = {
  args: {
    variant: 'solid',
    colorScheme: 'secondary',
    leftIcon: () => <CreateIcon />,
    label: 'Create',
  },
};

export const RightIcon: Story = {
  args: {
    variant: 'solid',
    colorScheme: 'secondary',
    rightIcon: () => <CreateIcon />,
    label: 'Create',
  },
};

export const Disabled: Story = {
  args: {
    variant: 'solid',
    label: 'Button',
    disabled: true,
  },
};

export const Loading: Story = {
  args: {
    variant: 'solid',
    loading: true,
  },
};
