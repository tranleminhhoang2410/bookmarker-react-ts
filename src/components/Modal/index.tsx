import { ReactNode } from 'react';
import { twMerge } from 'tailwind-merge';

export interface ModalProps {
  isOpen: boolean;
  children: ReactNode;
  backgroundColor?: string;
}

const Modal = ({ isOpen, children, backgroundColor = 'light-black' }: ModalProps) => {
  if (!isOpen) return null;

  const modalBaseClassName = 'fixed inset-0 flex items-center justify-center z-50';
  const modalBackgroundClassName = backgroundColor.includes('#') ? `bg-[${backgroundColor}]` : `bg-${backgroundColor}`;

  const modalClassName = twMerge(modalBaseClassName, modalBackgroundClassName);

  return <div className={modalClassName}>{children}</div>;
};

export default Modal;
