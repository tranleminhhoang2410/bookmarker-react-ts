import { Meta, StoryObj } from '@storybook/react';
import Modal from '@/components/Modal';

const meta: Meta<typeof Modal> = {
  title: 'Components/Modal',
  component: Modal,
  tags: ['autodocs'],
};

export default meta;

type Story = StoryObj<typeof Modal>;

export const Default: Story = {
  args: {
    isOpen: true,
    children: <h1>This is Modal content</h1>,
  },
};
