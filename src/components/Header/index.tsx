// External libraries
import { Link } from 'react-router-dom';

// Constants
import { ROUTES } from '@/constants';

// Icon
import { LogoIcon } from '../Icon';

// Component
import Typography from '../Typography';

const Header = () => {
  return (
    <header className="flex items-center justify-between p-8 border-b border-light-gray">
      <Link to={ROUTES.HOME}>
        <LogoIcon />
      </Link>
      <Typography variant="h1">Hello! Welcome to the Book Marker</Typography>
    </header>
  );
};

export default Header;
