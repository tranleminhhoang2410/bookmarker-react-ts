// Types
import { Icon } from '@/types';

const EditIcon = ({ size = 24, color = 'white' }: Icon) => (
  <svg width={size} height={size} viewBox="0 0 24 24" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
    <path d="M18 0L15 3L21 9L24 6L18 0ZM12 6L0 18V24H6L18 12L12 6Z" fill={color} />
  </svg>
);

export default EditIcon;
