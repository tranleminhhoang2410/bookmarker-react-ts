import { Meta, StoryObj } from '@storybook/react';

// Types
import { Icon } from '@/types';

// Components
import BrandingLogoIcon from './BrandingLogoIcon';
import BackIcon from './BackIcon';
import CalendarIcon from './CalendarIcon';
import CloseIcon from './CloseIcon';
import CreateIcon from './CreateIcon';
import DeleteIcon from './DeleteIcon';
import EditIcon from './EditIcon';
import SearchIcon from './SearchIcon';
import UploadIcon from './UploadIcon';

const meta: Meta<Icon> = {
  title: 'Components/Icon',
  argTypes: {
    children: { table: { disable: true } },
    size: {
      control: 'select',
      options: [4, 8, 12, 16, 20],
    },
    color: { control: 'color' },
  },
  tags: ['autodocs'],
};

export default meta;

type Story = StoryObj<Icon>;

const args: Partial<Icon> = {
  color: 'black',
};

export const BrandingLogo: Story = {
  render: (args) => <BrandingLogoIcon {...args} />,
};

export const Back: Story = {
  render: (args) => <BackIcon {...args} />,
  args,
};

export const Calendar: Story = {
  render: (args) => <CalendarIcon {...args} />,
  args,
};

export const Close: Story = {
  render: (args) => <CloseIcon {...args} />,
  args,
};

export const Create: Story = {
  render: (args) => <CreateIcon {...args} />,
  args,
};

export const Delete: Story = {
  render: (args) => <DeleteIcon {...args} />,
  args,
};

export const Edit: Story = {
  render: (args) => <EditIcon {...args} />,
  args,
};

export const Search: Story = {
  render: (args) => <SearchIcon {...args} />,
};

export const Upload: Story = {
  render: (args) => <UploadIcon {...args} />,
  args,
};
