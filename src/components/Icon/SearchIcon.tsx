// Types
import { Icon } from '@/types';

const SearchIcon = ({ size = 32, color = '#b5b5b5' }: Icon) => (
  <svg width={size} height={size} viewBox="0 0 32 32" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
    <path
      d="M13.8867 21.7733C18.2424 21.7733 21.7733 18.2424 21.7733 13.8867C21.7733 9.53098 18.2424 6 13.8867 6C9.53098 6 6 9.53098 6 13.8867C6 18.2424 9.53098 21.7733 13.8867 21.7733Z"
      fill="none"
      stroke={color}
      strokeWidth={3}
      strokeMiterlimit={10}
    />
    <path
      d="M19.9932 19.9933L25.7665 25.7667"
      fill="currentColor"
      stroke={color}
      strokeWidth={3}
      strokeMiterlimit={10}
      strokeLinecap="round"
    />
  </svg>
);

export default SearchIcon;
