// Types
import { Icon } from '@/types';

const CreateIcon = ({ size = 18, color = 'white' }: Icon) => (
  <svg xmlns="http://www.w3.org/2000/svg" width={size} height={size} fill="currentColor">
    <path stroke={color} strokeLinecap="round" strokeWidth={2.5} d="M1.5 9h15M9 1.5v15" />
  </svg>
);

export default CreateIcon;
