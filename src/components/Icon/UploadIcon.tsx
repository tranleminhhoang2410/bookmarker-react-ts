// Types
import { Icon } from '@/types';

const UploadIcon = ({ size = 24, color = 'white' }: Icon) => (
  <svg width={size} height={size} viewBox="0 0 24 24" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
    <path d="M0 0V3H24V0H0ZM12 6L3 15H9V24H15V15H21L12 6Z" fill={color} />
  </svg>
);

export default UploadIcon;
