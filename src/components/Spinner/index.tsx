import { twMerge } from 'tailwind-merge';

interface ISpinnerProps {
  size?: 'small' | 'medium' | 'large';
  color?: 'primary' | 'secondary' | 'light';
  className?: string;
}

const sizeClasses = {
  small: 'h-4 w-4',
  medium: 'h-6 w-6',
  large: 'h-8 w-8',
};

const colorClasses = {
  primary: 'border-t-primary',
  secondary: 'border-t-secondary',
  light: 'border-t-white',
};

const Spinner = ({ size = 'medium', color = 'primary', className = '' }: ISpinnerProps) => {
  const sizeClass = sizeClasses[size];
  const colorClass = colorClasses[color];

  const spinnerClass = twMerge('border-2 border-solid rounded-full animate-spin', sizeClass, colorClass, className);

  return <div className={spinnerClass}></div>;
};

export default Spinner;
