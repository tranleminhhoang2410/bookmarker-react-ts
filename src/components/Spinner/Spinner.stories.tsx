import { Meta, StoryObj } from '@storybook/react';

// Components
import Spinner from '@/components/Spinner';

const meta: Meta<typeof Spinner> = {
  title: 'Components/Spinner',
  component: Spinner,
  argTypes: {
    size: {
      control: {
        type: 'select',
        options: ['small', 'medium', 'large'],
      },
    },
    color: {
      control: {
        type: 'select',
        options: ['primary', 'secondary', 'danger', 'light'],
      },
    },
  },
  tags: ['autodocs'],
};

export default meta;
type Story = StoryObj<typeof Spinner>;

export const SmallPrimary: Story = {
  args: {
    size: 'small',
    color: 'primary',
  },
};

export const MediumSecondary: Story = {
  args: {
    size: 'medium',
    color: 'secondary',
  },
};

export const LargeLight: Story = {
  args: {
    size: 'large',
    color: 'light',
  },
};

export const CustomClass: Story = {
  args: {
    size: 'medium',
    color: 'primary',
    className: 'border-t-gray-blur',
  },
};
