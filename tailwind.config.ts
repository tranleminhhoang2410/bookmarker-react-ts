import type { Config } from 'tailwindcss';

export default {
  content: ['./src/**/*.{html,ts,tsx}'],
  theme: {
    screens: {
      sm: '36em',
      md: '48em',
      lg: '62em',
      xl: '75em',
    },
    fontFamily: {
      primary: 'Lato, sans-serif',
      secondary: 'Tiny5, sans-serif',
    },
    fontSize: {
      sm: '1rem',
      md: '1.25rem',
      lg: '2rem',
      xl: '2.25rem',
    },
    extend: {
      colors: {
        primary: '#fabb18',
        secondary: '#000',
        white: '#fff',
        placeholder: '#b5b5b5',
        danger: '#fa4848',
        success: '#6ac83c8a',
        fail: '#c83c3c8a',
        'white-blur': '#f4f4f4',
        'gray-blur': '#d5d5d5',
        'primary-loading': '#d9d9d9d',
        'light-gray': '#ccc',
        'light-black': '#65656591',
      },
      borderRadius: {
        xss: '0.5rem',
        xs: '0.625rem',
        sm: '1rem',
        md: '1.25rem',
        lg: '1.875rem',
      },
    },
  },
  plugins: [],
} satisfies Config;
